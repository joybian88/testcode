#include<stdio.h>
#include<stdlib.h>

void constPtr()
{
    int a = 10;
    int *b = NULL;
    int **c = NULL;
    b = &a;
    c = &b;
    printf("a=%d b=%d c =%d \n",a,*b,**c);
}

void shuzuPtr()
{
    int i = 0;
    int j = 0;
    int a[3] = {1,2,3};
    int b[3] = {4,5,6};
    int *c = NULL;
    int *d = NULL;
    c = a;
    d = b;
    for(i=0;i<3;i++)
    {
        printf("c[%d]=%d \n",i,c[i]);
    }
    int **e = (int **)malloc(2*sizeof(int *));
    e[0] = c;
    e[1] = d;
    
    for(i=0;i<2;i++)
    {
        for(j=0;j<3;j++)
        {
            printf("e[%d][%d]=%d \n",i,j,e[i][j]);
        }
    }
    
}

void structPtr()
{
    int i =0;
   typedef struct node
   {
       int data ;
       struct node *ptNext;
   }T_Node;
   
   T_Node *a = (T_Node *)malloc(3*sizeof(T_Node));
   T_Node *b = (T_Node *)malloc(3*sizeof(T_Node));
   
   for(i=0;i<3;i++)
   {
       (a+i)->data = i+100;
       a[i].ptNext = NULL;
       (b+i)->data = i+200;
       b[i].ptNext = NULL;
   }
   for(i=0;i<3;i++)
   {
       printf("a[%d].data=%d b[%d].data=%d \n",i,a[i].data,i,b[i].data);
   }
   
   T_Node **c = (T_Node **)malloc(2*sizeof(T_Node *));
   c[0] = a;
   c[1] = b;
   
   c[0]->ptNext = a+1;
   c[0]->ptNext->ptNext = a+2;
   c[1]->ptNext = b+1;
   c[1]->ptNext->ptNext = b+2;
   
   printf("%d \n",c[0]->data);
   printf("%d \n",c[0]->ptNext->data);
   printf("%d \n",c[0]->ptNext->ptNext->data);
   
   
}

void main()
{
    constPtr();
    printf("************ \n");
    shuzuPtr();
    printf("************ \n");
    structPtr();
}